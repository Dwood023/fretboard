import { Scale, Note, Distance, Chord } from "tonal";

export function note_at_fret(string_tuning, fret) {
    const 
        all_notes = Note.names(" b"), 
        start = all_notes.findIndex( 
            note => equivalent(note, string_tuning)
        );

    return all_notes[(start + fret) % all_notes.length];

}

export function equivalent(note_a, note_b) {
    const distance = Distance.semitones(note_a, note_b);
    return distance === 0;
}

export function parse_symbol(root, symbol) {
    if (Chord.exists(symbol)) {
        return parse_chord(root, symbol);
    } else if (Scale.exists(symbol)) {
        return parse_scale(root, symbol);
    } else {
        throw `Could not parse ${symbol} as chord or scale`;
    }
}

function parse_chord(root, chord) {
    // FIXME -- Some of these are broken (eg. 4, 64)
    // TODO - Perhaps a smaller list, sorted into categories and sorted by relevance
    const notes = Chord.notes(root, chord);
    const intervals = Chord.intervals(chord);

    const note_data = [];

    for (let i = 0 ; i < notes.length ; i++) {
        note_data.push(
            {
                name: notes[i],
                interval: intervals[i]
            }
        )
    }

    return note_data
}

function parse_scale(root, scale) {
    const notes = Scale.notes(root, scale);
    const intervals = Scale.intervals(scale);

    const note_data = [];

    for (let i = 0 ; i < notes.length ; i++) {
        note_data.push(
            {
                name: notes[i],
                interval: intervals[i]
            }
        )
    }

    return note_data;
}

export const chord_list = Chord.names();
export const note_list = Note.names(" b");
export const scale_list = Scale.names();