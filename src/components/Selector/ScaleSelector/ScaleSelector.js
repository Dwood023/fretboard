import React from 'react';
import { scale_list } from "../../../theory";

function ScaleSelector(props) {

    const scales = scale_list.map(
        scale => <option key={scale} value={scale}>{scale}</option>
    );

    return (
        <select 
            className="ScaleSelector" 
            name="scale" 
            onChange={props.OnChange}
            defaultValue=""
        >
            <option key="" value="" disabled>Select scale</option>
            {scales}       
        </select>
    )
}

export default ScaleSelector;