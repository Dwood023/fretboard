import React from 'react';
import { chord_list } from "../../../theory";

function ChordSelector(props) {

    const chord_types = chord_list.map(
        type => <option key={type} value={type}>{type}</option>
    );

    return (
        <select 
            className="ChordSelector" 
            name="chord" 
            onChange={props.OnChange}
            defaultValue=""
        >
            <option key="" value="" disabled>Select chord</option>
            {chord_types}       
        </select>
    )
}

export default ChordSelector;