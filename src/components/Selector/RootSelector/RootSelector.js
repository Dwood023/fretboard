import React from 'react';
import { note_list } from "../../../theory";

function RootSelector(props) {

    const notes = note_list.map(
        note => <option key={note} value={note}>{note}</option>
    );

    return (
        <select 
            className="RootSelector" 
            name="root" 
            onChange={props.OnChange}
            defaultValue=""
        >
            <option key="" value="" disabled>Select root note</option>
            {notes}
        </select>
    );
}

export default RootSelector;