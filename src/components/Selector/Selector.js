import React, { Component } from 'react';
import "./Selector.css";

import RootSelector from './RootSelector/RootSelector';
import ChordSelector from './ChordSelector/ChordSelector';
import VariantSelector from './VariantSelector/VariantSelector';
import ScaleSelector from './ScaleSelector/ScaleSelector';

/*
    Offer options for Chords and Modes
*/
class Selector extends Component {
    constructor(props) {
        super(props);
        // So window can call Selector.handleChange
        this.symbol_changed = this.symbol_changed.bind(this);
        this.root_changed = this.root_changed.bind(this);
        this.switch_variant = this.switch_variant.bind(this);

        this.state = {
            variant: "chord" // Toggles between chord/mode selector
        };

        this.selected = {
            root: "",
            symbol: "",
        };
    }

    symbol_changed(event) {
        const selector = event.target;
        this.selected["symbol"] = selector.value;
        this.props.SelectionChanged(this.selected);
    }

    root_changed(event) {
        const selector = event.target;
        this.selected["root"] = selector.value;
        this.props.SelectionChanged(this.selected);
    }

    // To switch between chord/mode selector
    switch_variant(event) { 
        this.setState({
             variant: event.target.value 
        })
    }

    render() {

        switch (this.state.variant) {
            default:
            case "chord":
                this.symbol_selector = (
                    <ChordSelector
                        OnChange={this.symbol_changed}
                    />
                ); break;
            case "scale":
                this.symbol_selector = (
                    <ScaleSelector
                        OnChange={this.symbol_changed}
                    />
                ); break;
        }

        return (
            <div className="Selector">
                <VariantSelector 
                    OnChange={this.switch_variant}
                />
                <RootSelector
                    OnChange={this.root_changed}
                />
                { this.symbol_selector }
            </div>
        )
    }
}
        
export default Selector;