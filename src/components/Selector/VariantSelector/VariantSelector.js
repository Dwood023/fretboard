import React from 'react';

function VariantSelector(props) {

    return (
        <select className="VariantSelector" name="variant" onChange={props.OnChange}>
            <option key="chord" value="chord">Chord</option>
            <option key="scale" value="scale">Scale</option>
        </select>
    );
}

export default VariantSelector;