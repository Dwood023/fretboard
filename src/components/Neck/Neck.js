import React from 'react';
import './Neck.css';

import { get_fret_positions } from "./fret_positions";

import Fretboard from "./Fretboard/Fretboard";

function Neck(props) {

    const 
        scale_length = props.ScaleLength,
        fret_count = props.FretCount;

    const fret_positions = get_fret_positions(fret_count, scale_length);

    const 
        length = fret_positions[fret_positions.length - 1] + 1,
        width = props.NeckWidth;

    // Fit to the fretboard, plus some margin 
    const view_box = `-0.5 0 ${length + 0.7} ${width}`;

    return (
        <svg viewBox={view_box} shapeRendering="geometricPrecision">
            <rect 
                className="Neck" 
                width={length} 
                height={width} 
            />
            <Fretboard 
                ScaleLength={scale_length} 
                FretCount={fret_count} 
                NeckWidth={width} 
                FretPositions={fret_positions}
                ActiveNotes={props.ActiveNotes}
            />
        </svg>
    )
}

export default Neck;