export function get_string_heights(neck_width) {

    const string_count = 6;
    const string_area = neck_width / string_count;
    const half = string_area / 2;

    return Array.from(Array(string_count).keys())
        .map(
            (n) => (string_area * n+1) - half
        )
}