import React from 'react';
import './String.css';

import Note from './Note/Note';

import { note_at_fret, equivalent } from "../../../../../theory";

function String(props) {

    const 
        base_thickness = 0.20,
        scaling_factor = 0.02;

    const 
        diameter = base_thickness - ((props.Num) * scaling_factor),
        radius = diameter / 2,
        y_pos = props.Height + radius,
        center = y_pos + radius;

    const note_data = props.NotePositions.reduce( 
        function(output, x_pos, fret) {

            const found = props.ActiveNotes.filter( 
                active_note => equivalent(note_at_fret(props.Tuning, fret), active_note.name)
            )[0]; // Should only return one (assume no enharmonic duplicates in ActiveNotes)

            // If fret has found itself in ActiveNotes
            if (found !== undefined) { 
                output.push({ 
                    name: found.name,
                    interval: found.interval,
                    fret,
                    x_pos
                })
            }

            return output;
        }, []
    );

    const notes = note_data.map(
        (note, n) =>
            <Note
                key={`${props.Num}-${note.fret}`}
                Name={note.name}
                Interval={note.interval}
                XPos={note.x_pos}
                YPos={center} 
            />
    )

    return (
        <g>
            <rect className="String" y={y_pos} width={props.Length} height={diameter}/>
            <g id="notes">
                { notes }
            </g>
        </g>
    )
}

export default String;