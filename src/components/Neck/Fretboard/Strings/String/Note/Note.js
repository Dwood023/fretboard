import React from 'react';
import './Note.css';

import CenteredText from './CenteredText/CenteredText';

function Note(props) {

    return (
        <g className={`Note ${props.Interval}`} >
            <circle 
                cx={props.XPos} 
                cy={props.YPos}
                r={.35}
            />
            <CenteredText 
                XPos={props.XPos} 
                YPos={props.YPos}
            >
                {props.Name}
            </CenteredText>
        </g>
    )
}

export default Note;