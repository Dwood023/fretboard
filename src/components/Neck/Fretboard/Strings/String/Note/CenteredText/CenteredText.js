import React from 'react';
import './CenteredText.css';

export function CenteredText(props) {
    return (
        <text
            x={props.XPos}
            y={props.YPos}
            textAnchor="middle"
            dy="0.36em"
        >
            {props.children}
        </text>
    )
}
export default CenteredText;