import React from 'react';
import String from './String/String';
import { get_string_heights } from './string_positions';

function Strings(props) {

    const  
        scale_length = props.ScaleLength,
        note_positions = props.NotePositions,
        neck_width = props.NeckWidth;

    const string_positions = get_string_heights(neck_width);

    const tunings = ["E", "A", "D", "G", "B", "E"];

    const strings = string_positions.map(
        (y_pos, n) =>
            <String 
                key={6 - n}
                Num={6 - n}
                Tuning={tunings[5-n]}
                Height={y_pos}
                Length={scale_length} 
                NotePositions={note_positions}
                ActiveNotes={props.ActiveNotes}
            />
    );

    return (
        <g id="strings">
            {strings}
        </g>
    )
}

export default Strings;