import React from "react";
import Fret from "./Fret/Fret"

export function Frets(props) {

    const
        fret_positions = props.FretPositions,
        neck_width = props.NeckWidth;

    const frets = fret_positions.map(
        (x_pos, n) => 
            <Fret 
                key={n} 
                XPos={x_pos} 
                Width={neck_width} 
            /> 
    );

    return (
        <g id="frets"> 
            { frets }
        </g>
    )
}

export default Frets;