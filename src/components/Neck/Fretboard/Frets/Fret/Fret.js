import React from 'react';
import './Fret.css'

function Fret(props) { 
    const width = 0.12;
    return (
        <rect 
            className="Fret" 
            x={props.XPos - (width / 2)} 
            width={width} 
            height={props.Width} 
        />
    )
}

export default Fret;