import React from 'react';
import './Fretboard.css';

import { get_note_positions } from "./note_positions";

import Inlays from './Inlays/Inlays';
import Frets from './Frets/Frets';
import Strings from './Strings/Strings';

function Fretboard(props) {
    const 
        scale_length = props.ScaleLength,
        fret_positions = props.FretPositions,
        neck_width = props.NeckWidth;

    const note_positions = get_note_positions(fret_positions);

    return (
        <g id="Fretboard">
            <Inlays
                NeckWidth={neck_width}
                NotePositions={note_positions}
            />
            <Frets
                FretPositions={fret_positions}
                NeckWidth={neck_width}
            />
            <Strings
                NeckWidth={neck_width}
                ScaleLength={scale_length}
                NotePositions={note_positions}
                ActiveNotes={props.ActiveNotes}
            />
        </g>
    )
}

export default Fretboard;