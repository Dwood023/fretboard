export function get_note_positions(fret_positions) {
    let positions = [];

    positions.push(0); // Open string position

    for (let n = 1 ; n < fret_positions.length ; n++) {
        let fret_area = fret_positions[n] - fret_positions[n-1];
        let halfway = fret_positions[n] - (fret_area / 2);

        positions.push(halfway);
    }

    return positions;
}