
export function get_inlay_positions(midfret_positions) {
    return midfret_positions.filter(
        (x_pos, n) => has_inlay(n) 
    );
}

function has_inlay(fret_num) {
    // No even frets have inlays
    const is_odd = fret_num % 2 !== 0;
    // With the exception of fret 12, 24, etc.
    const multiple_of_12 = fret_num % 12 === 0;
    // Zero fret doesn't need inlay
    const not_zero = fret_num !== 0;
    // Also make sure its not adjacent to multiple of 12 (1, 11, 13, etc.)
    const not_exception = ![1, 11].includes(fret_num % 12);

    return (is_odd && not_exception) || (multiple_of_12 && not_zero);
}
