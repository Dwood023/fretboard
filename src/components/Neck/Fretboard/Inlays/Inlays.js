import React from 'react';
import { get_inlay_positions } from "./inlay_positions";
import Inlay from "./Inlay/Inlay";

export function Inlays(props) {
    const
        note_positions = props.NotePositions,
        neck_width = props.NeckWidth;

    const inlay_positions = get_inlay_positions(note_positions);

    const inlays = inlay_positions.map( 
        (x_pos, n) => 
            <Inlay 
                key={n}
                XPos={x_pos}
                NeckWidth={neck_width}
                Double={(n+1) % 5 === 0} 
            /> 
    );

    return (
        <g id="inlays">
            { inlays }
        </g>
    )

}

export default Inlays;