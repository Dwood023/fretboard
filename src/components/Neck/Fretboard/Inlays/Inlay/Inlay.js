import React from 'react';

function Inlay(props) {

    if (props.Double === true) {
        return (
            <g>
                <circle 
                    cx={props.XPos}
                    cy={props.NeckWidth / 3} 
                    r="0.3" 
                />
                <circle 
                    cx={props.XPos} 
                    cy={(props.NeckWidth / 3) * 2} 
                    r="0.3"
                />
            </g>
        )
    }
    else return (
        <circle 
            cx={props.XPos} 
            cy={props.NeckWidth / 2} 
            r="0.3" 
        />
    )
}

export default Inlay;