export function get_fret_positions(fret_count, scale_length) {
    // Hmmm is this really neater than for-loop?
    return Array.from(Array(fret_count).keys())
        .map(
            (x, n) => fret_pos(scale_length, n)
        );
}

function fret_pos(scale_length, fret_num) {
    // Uhh wtf is this
    // TODO - Explain in plain english
    const divisor = (2 ** fret_num) ** (1 / 12);
    // This is the length of string yet free to play (if fret is pressed)
    const bridge_distance = scale_length / divisor;
    // Length of non-vibrating section, distance to nut
    return scale_length - bridge_distance;
}
