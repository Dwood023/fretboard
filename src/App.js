import React, { Component } from 'react';
import './App.css';
import { parse_symbol } from "./theory";

import Neck from "./components/Neck/Neck"
import Selector from "./components/Selector/Selector"

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			active_notes: []
		}
		// So children can call function referring to this
		this.parse_notes = this.parse_notes.bind(this);
	}

	// When user requests a chord or mode
	// request could be
	// Chord string "Gm7b5"
	// Mode string "A lydian"
	// FIXME - only parsing chords currently
	parse_notes(selected) {

		const notes = parse_symbol(selected.root, selected.symbol);

		this.setState({
			active_notes: notes
		})
	}

	render() {
		return (
			<div className="main">
				<div className="FretboardDiv">
					<Neck
						ScaleLength={62}
						FretCount={13}
						NeckWidth={7}
						ActiveNotes={this.state.active_notes}
					/>
					<Selector 
						SelectionChanged={this.parse_notes} 
					/>
				</div>
			</div>
		);
	}
}

export default App;
